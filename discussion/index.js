//one line comment(ctrl + slash); (mac: cmd + /)
/*
  multiline comment (ctrl + shift + slash); (mac: cmd + option + /)
  */


  console.log("Hello Batch 170!");

  /*
	JS - we can see the messages/log in the console.

	Browser Consoles are part of the browsers w/c will allow us to see or log the messages, 
	data or info fr the programming language
			- they are easily accessed in the dev tools

	Statements
		instructions, expressions that we add to the programming language to communicate w/
		the computers

		usu. ending w/ a semicolon(;). However, JS has implemented a way to automatically add
		semicolons at the end of the line

	Syntax		
		set of rules that describes how stmts shd be made/constructed

		lines/blocks of codes must follow a certain set of rules for it to work properly	
  */

  console.log("Reese")

  /*  
	create 3 console logs to display your favorite food
		console.log("<yourFavoriteFood>")


  */

  /* console.log("pizza");
  console.log("salad");
  console.log("pasta"); */


 let food1 = "Adobo"

 console.log(food1);


 /*
	Variables are way to store info or data w/in the JS.

	to create a variable, we 1st declare the name of the variable w/ either let/const keyword:

		let nameOfVariable

	Then, we can initialize the variable w/ a value or data	

		let nameOfVariable = <valueOfVariable>


 */


 	console.log("My favorite food is: " + food1);

 	let food2 ="Kare Kare";

 	console.log("My favorite foods are: " + food1 + "and " + food2);

 	let food3;

 	/*   
		we can create variables w/o values, but the variables' content would log undefined.

 	*/

 	console.log(food3)
 	

 	food3 = "Chickenjoy";
 	/* 
		we can update the content of a variable by reassigning the value using an assignment 
		operator (=);

		assignment operator(=) lets us (re)assign values to a variable
	*/
 	console.log(food3)


 	/* we cannot creaate another variable w/ the same name. It will result
 		in an error */

 	/* let food1 ="Flat Tops"

 	console.log(food1) */

 	//const keyword

 	/*
		const keyword will also allow creation of variable. However, w/ a const
		keywork, we create constant variable, w/c means the data saved in these 
		variables will not change, cannot change & shd not be changed
 	*/

 	const pi = 3.1416;

 	console.log(pi);

 	/* pi = "Pizza"

 	console.log(pi) */

 	

 	/* 
		we also cannot create a const variable w/o initialization or assigning its 
		value.

 	const gravity;

 	console.log(gravity);
	*/



	/*
		Mini activity

		LET KEYWORD
		reassign a new value for  c w/ another fave food of yours 
		reassign a new value for food2 w/ another fave food of yours 
		log the values of both variables in the console

		CONST KEYWORD
		create a const variable called sunriseDirection w/ East as its value
		create a const variable called sunsetDirection w/ West as its value
		log the values of both variables in the console
	*/

		food1 = "panera";
		food2 = "mccallisters";
		console.log(food1);
		console.log(food2);

		const sunriseDirection = "East";
		const sunsetDirection = "West";
		console.log(sunriseDirection);
		console.log(sunsetDirection);


		/*
			Guidelines in creating a JS variable

			1. We can create a let variable w/ the let keyword. let variable can be
				reassigned but not redeclared

			2. Creating a variable has 2 parts: Declaration of the variable name &
				initialization of the initial value of the variable through the use
				of assignment operator (=)	

			3. A let variable can be declared w/o initialization. However, the value of
				the variable will be undefined until it is re-assigned w/ a value

			4. Not Defined cs Undefined; Not Defined error means that the variable is used 
				but not DECLARED. Undefined results fr a variable that is used but not
				INITIALIZED

			5. We can use const keyword to create variables. Constant variables cannot be
				declared w/o initialization. Constant variables values cannot be reassigned

			6. When creating variable names, start w/ small caps, this is bec. we are 
				avoiding conflict w/ JS that is named w/ capital letters

			7. If the variable would need 2 words, the naming convention is the use of 
				camelCase. Do not add any space/s for the variables w/ words as names

		*/



		// Data Types

		/*
			strings
				are data types w/c are alphanumerical txt. They could be a name, phrase,
				or even a sentence. We can create string data types w/ a single quote (')
				or w/ a double (''). The txt inside the quotation marks will be displayed
				as it is

			keyword variable = "string data";	
		*/

		console.log("Sample String Data");


		/*
			numeric data types
				are data types w/c are numeric. numeric data types are displayed when
				nos. are not placed in the quotation marks. If there is mathematical 
				operation needed to be done, numeric data type shd be used instead of
				string data type

			keyword variable = numeric data;
		*/

		console.log(0123456789);
		console.log(1+1)


		// Mini activity

		/*
			using string data type
				display in the console your:
				name(first&last)
				bday
				province where you live

			using numeric data type	
				display in the console your:
					high score in the last game you played
					fave no.
					your fave no. in electric fan/aircon
		*/

		console.log("Reese Salanga");
		console.log("Nov20");
		console.log("Bulacan & La Union")
		

		console.log(2408);
		console.log(7);
		console.log(2);
