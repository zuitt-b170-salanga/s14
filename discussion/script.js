// JS Fundtions
/*
	Functions are used to create reuseable commands/stmts that prevents the dev
	fr typing a bunch of codes. In field, a big number of lines of codes is the 
	normal output; using functions would save the dev a lot of time & effort in
	typing the codes that will be used multiple times

	SYNTAX
	function functionName(parameters){
		command/stmt
	}

*/

function printStar(){
	console.log("*")
}


printStar();
printStar();
printStar();
printStar();

function sayHello(name){
	/*
		functions can also use parameters. These parameters can be defined & a 
		part of the cmd inside the function. When called, parameters can be 
		replaced w/ the target value of the dev. Make sure that the value is 
		inside quotations when called
	*/
	console.log("Hello " + name)
}

sayHello("Reese");


/*  function alertPrint(){
	alert("Hello");
	console.log("Hello")
};
alertPrint(); */

// Function that accepts 2 nos. & prints the sum
	function add(x,y){

		let sum = x + y
		console.log(sum)
	}

	add(1,2);
	add(10,44);
	add(8,20);
	add(831,440,123)
	// add(831,440,123): if the no. of parameters defined exceeds the needed, the 
	//	excess would be ignored by JS

	// 3 parameters
	// display the fname, lname, age

	function printBio(fname, lname, age){
		// console.log("Hello " + fname + " " + lname + " " + age)

		//using template literals
		/*
			declared by the use of backticks w/ dollar sign & curly braces w/ the
			parameters inside the braces. The displayed data in the console will 
			be defined be the defined parameter instead of the txt inside the 
			curly braces
		*/
		//backticks - the symbol on the left side of the 1 in the keyboard
		console.log(`Hello ${fname} ${lname} ${age}`)
	};


	printBio("Marco", "Kalalo", 12);

	function createFullName(fname, mname, lname){
		// return specifies the value to be given back by the function once it is 
		//finished executing. The value can be given to a variable
		return `${fname} ${mname} ${lname}`
	};

	let fullName = createFullName("Juan", "Dela", "Cruz");
		console.log(fullName);