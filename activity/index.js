// displayed using the regular console.log("")

console.log("Hello, World!");

let fName = "John";
let lName = "Smith";
let hisAge = 30;

	console.log("First Name: " + fName);
	console.log("Last Name: " + lName);
	console.log("Age: " + hisAge);

const hobby1 = "Biking";
const hobby2 = "Mountain Climbing";
const hobby3 = "Swimming";
let q = '"';
let c = ',';
let bl = '[';
let br = ']';
let cbl = '{';
let cbr = '}';
	console.log("Hobbies: ");
 	console.log(bl + q + hobby1 + q + c + " " 
		+ q + hobby2 + q + c + " " + q + hobby3 + q + br);

    console.log("Work Address: ");
    console.log(" ");
    console.log(cbl + "houseNumber: " + q + 32 + q + c + " street: " + 	q + "Washington" + q + c 
    	+ " city: " + q + "Lincoln" + q + c + " state: " + q + "Nebraska" + q + cbr);

// displayed using the regular console.log("")





// displayed using functions

	function printUserInfo(fName, lName, hisAge){
		console.log(`${fName} ${lName} is ${hisAge} years of age.`);
		console.log("This was printed inside of the function");
		console.groupCollapsed(bl + q + hobby1 + q + c + " " 
		+ q + hobby2 + q + c + " " + q + hobby3 + q + br);
		console.groupEnd();
		console.log("This was printed inside of the function");
		console.log(" ");
		console.groupCollapsed(cbl + "houseNumber: " + q + 32 + q + c + " street: " + q + "Washington" 
			+ q + c + " city: " + q + "Lincoln" + q + c + " state: " + q + "Nebraska" + q + cbr);
		console.groupEnd();
	};
	printUserInfo("John", "Smith", 30);
	


	function returnFunction(){
	const isMarried = true;
		return isMarried;
	}
	let status = returnFunction();
	console.log("The value of isMarried is: " + status);

// displayed using functions


	// 	const isMarried = new Boolean(true);
	// 	return isMarried.toString();
	// }
	// let status = returnFunction();
	// 	console.log("The value of isMarried is: " + status);